#This file was created by Yombo for use with Yombo Gateway automation
#software.  Details can be found at http://www.yombo.net
"""
MythTV
======

Interface to MythTV frontends. Stop, play, pause, etc.

License
=======

See LICENSE file for full details.

The **Yombo** team and other contributors hopes that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See LICENSE file for full details.

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: Copyright 2012 by Yombo.
:license: See LICENSE file for details.
:organization: `Yombo <http://www.yombo.net>`_
"""
from collections import deque
from re import sub

from twisted.internet import reactor
from twisted.internet.protocol import Protocol
from twisted.internet.protocol import ReconnectingClientFactory

from yombo.core.module import YomboModule
from yombo.core.log import getLogger


logger = getLogger()
    
class MythTV(YomboModule):
    """
    MythTV class.... blah
    """
    def __init__(self):
        """
        Setup the class.  Each myth tv device will have a seperate connection
        make to it.
        """
        YomboModule.__init__(self)
        self._ModDescription = "Interface to MythTV API"
        self._ModAuthor = "Mitch Schwenk & Lance DeMello"
        self._ModUrl = "http://www.yombo.net"

#        self._RegisterDistributions = ["cmd", "status"]
#        self._RegisterVoiceCommands = [{'voiceCmd':"[stop all, pause all, play all] myth",
#                                                'order' : 'verbnoun'}]
        self.ourDeviceType = 'EZb4BYDicT86RX4AYZhTcDSW' # = mythtv frontend device type...
        self.ourDeviceIDs = []
        self.mythcommands = {
            'stop' : 'play stop.... hehe' }

        self.messageStore = {}  # store messages so we can return results when done.

        self.connectionStatus = {}  # a place to store/link connections

    def load(self):
        """
        Load gets all mythtv devices listed and prepares start() for connecting
        to various mythtv front ends.
        """
        self.ourDeviceIDs = self._DevicesByType(self.ourDeviceType)
        logger.debug("outdeviceids: %s", self.ourDeviceIDs)
        if (len(self.ourDeviceIDs) == 0):
            logger.warning("MythTV module installed, but no MythTV frontends defined.")
            return
        for deviceID in self.ourDeviceIDs:
            address = self._devices[deviceID].deviceaddress.split(":")
            if len(address) == 1:
                address[1] = 6546
            self.connectionStatus[deviceID] = {'connecting' : False, # if attempting to connect
                                               'pingable'   : False, # test if we can ping before trying to connect
                                               'connection' : False, # the connection
                                               'factory'    : None, # represent factory for connected mythtv
                                               'reactor'    : None, 
                                               'address'    : str(address[0]),  #ip address of frontend
                                               'port'       : int(address[1]),  #port to conenct to
                                              }

        for deviceID in self.ourDeviceIDs:  ## lets pretend everything is pingable for now..
            #@todo: do a ping test...
            self.connectionStatus[deviceID]['pingable'] = True

        logger.debug("connection status: %s", self.connectionStatus)


    def start(self):
        """
        Opens a connection to mythtv and gets the connection ready for use.
        
        Outline:
        # open connection socket to mythtv
        #reference: http://www.mythtv.org/wiki/Telnet_socket#Limitations_.26_Workarounds
        #reference: http://twistedmatrix.com/documents/current/core/howto/clients.html
        """
        if (len(self.ourDeviceIDs) == 0):
            logger.warning("Not starting MythvTV modules since there are no frontends defined.")
            return

        for deviceID in self.ourDeviceIDs:
            if self.connectionStatus[deviceID]['pingable']:
                logger.debug("connection details: %s", self.connectionStatus[deviceID])
                self.connectionStatus[deviceID]['reactor'] = reactor.connectTCP(self.connectionStatus[deviceID]['address'],
                    self.connectionStatus[deviceID]['port'],
                    MythTVFactory(self, deviceID))

    def stop(self):
        """
        @todo: Close the TCP connections...
        """
        pass

    def unload(self):
        pass
    
    def connected(self, deviceid, endpoint):
        self.connectionStatus[deviceid]['connection'] = endpoint
        
    def message(self, message):
        """
        Got a message from Yombo Gateway system, might be a command...status...etc.
        """
        logger.debug("MythTV got message")
        if message.payload['deviceid'] not in self.ourDeviceIDs:
            # do nothing if it's not for us!
            return

        # if we are not connected, then we can't do anything. Send an error.
        if self.connectionStatus[message.payload['deviceid']]['connection'] == False:
          replmsg = message.getReply()
          replmsg.msgStatus = 'failed'
          replmsg.msgStatusExtra = 'Not connected to mythTV frontend.'
          replmsg.payload = {'cmd'     : message.payload['cmd'],
                             'deviceid': message.payload['deviceid'] }
          logger.debug("FAILED Request: %s", replmsg.dump())
          replmsg.send()
          return          

        # store the message for later..Used to send replies back to sender.
        self.messageStore[message['msgID']] = message

        if message.msgType == 'cmd' and message.msgStatus == 'new':
            self.processNewCmdMsg(message)
        elif message.msgType == 'status':
            self.processNewStatusMsg(message)
        elif message.msgType == 'event':
            self.processNewEventMsg(message)
            
    def processNewCmdMsg(self, message):
        logger.debug("myth- in processnewcmdmsg")
        deviceid = message['payload']['deviceid']
        self.connectionStatus[deviceid]['connection'].processCommand(message)
    
    def processNewStatusMsg(self, message):
        pass

    def processNewEventMsg(self, message):
        pass

    def dataReceived(self, msgID, deviceID, status, data):
        """
        Data from a mythtv frontend will be delievered here,
        along with it's deviceid.
        """
        logger.debug("mytvclass dataReceived. msgID:%s, Deviceid:%s, data:%s", msgID, deviceID, data)

        replmsg = self.messageStore[msgID].getReply()
        replmsg.msgStatusExtra = 'done'
        replmsg.payload = {'status'  : data[0],
                           'cmd'     : self.messageStore[msgID]['payload']['cmd'],
                           'deviceid': deviceID }
        logger.debug("msgreply: %s", replmsg.dump())
        replmsg.send()
        del self.messageStore[msgID]


class MythTVProtocol(Protocol):
    def __init__(self, factory):
        self.name = "MythTVProtocol"
        self.fname = "yombo.modules.MythTVProtocol"
        self.factory = factory   # how to send messages back to mythclass.
        self.havePrompt = False
        self.inBuffer = ''   # for lineReceived, looking for prompt
        self.queue = deque({})
        self.pendingID = ''   # msgID of the pending command.  Waiting for response from frontend.
        self.dataLines = []   # used to hold all data for a given command.

        self.commandMap = {'stop'   : "play stop",
                           'play'   : "play speed normal",
                           'pause'  : "play speed pause",
                           'slow'   : "play speed 1/2x",
                           'skip-forward' : "play seek forward",
                           'skip-backward' : "play seek backward",
                           'seek-beginning' : "play seek beginning",
                           'volume-up' : "play volume up",
                           'volume-down' : "play volume down",
                           'channel-up' : "play channel up",
                           'channel-down' : "play channel down",
                           'query' : "query",
                           'jump' : "jump",
                           'key' : "key",
                           'exit' : "exit" }

    def connectionMade(self):
        self.factory.connected(self)

    def processCommand(self, message):
        payload = message['payload']
        cmd = payload['cmd']
        if cmd in self.commandMap:
            sendCmd = self.commandMap[cmd]
            if 'value1' in payload and payload['value1'] != "":
              sendCmd = sendCmd + " " + payload['value1']
              if 'value2' in payload and payload['value2'] != "":
                sendCmd = sendCmd + " " + payload['value2']
                if 'value3' in payload and payload['value3'] != "":
                  sendCmd = sendCmd + " " + payload['value3']
                  if 'value4' in payload and payload['value4'] != "":
                    sendCmd = sendCmd + " " + payload['value4']
            self.queue.appendleft({'msgID': message['msgID'], 'cmd': sendCmd})
            self.checkQueue()

    def checkQueue(self):
        """
        Check if there is a send queue.  If so, send something to mythtv front end.
        Send command to mythtv front end.
        """
        if self.havePrompt == True:
            if len(self.queue) > 0:
                self.havePrompt = False
                item = self.queue.pop()
                self.pendingID = item['msgID']
                self.sendMessage(item['cmd'])

    def sendMessage(self, cmd):
        """
        Send command to mythtv front end.
        """
        logger.debug("Sending to mythtv: %s", cmd)
        self.transport.write(cmd + "\n")

    def dataReceived(self, data):
        """
        Translates bytes into lines, and calls lineReceived.

        Also translates "# " into "# \r" to note mythtv is ready for another
        command.
        """
        logger.debug("got data: %s", data)
        delimiter = '\r'
        self.inBuffer = self.inBuffer + data
        self.inBuffer = sub(r'\r# ', '\r# \r', self.inBuffer)
        
        lines = self.inBuffer.split(delimiter)
        self.inBuffer = lines.pop(-1)
        logger.debug("lines= %s", lines)
        logger.debug("buffer= %s", self.inBuffer)

        for line in lines:
            if self.transport.disconnecting:
                # this is necessary because the transport may be told to lose
                # the connection by a line within a larger packet, and it is
                # important to disregard all the lines in that packet following
                # the one that told it to close.
                return
            self.lineReceived(line)
            
    def lineReceived(self, data):
        """
        Received a string from a MythTV frontend
        """
        logger.debug("lineReceived: %s", data)
        if data != '# ':
            self.havePrompt = False
            self.dataLines.append(data)
        else:
            self.havePrompt = True
            if self.pendingID != '':  # this might drop data! Only send if we are waiting for a command.
                self.factory.dataReceived(self.pendingID, "unknown", self.dataLines)
                self.pendingID = ''
            self.dataLines = []
            self.checkQueue()

class MythTVFactory(ReconnectingClientFactory):
    protocol = MythTVProtocol
    maxDelay = 180
    factor = 2.2
#    jitter = 0.25

    def __init__(self, mythclass, deviceid):
        self.mythclass = mythclass
        self.deviceid = deviceid
        self.maxDelay = 60

    def startedConnecting(self, connector):
        self.maxDelay = 60
        logger.info("Started connecting to MythTV frontend.")

    def buildProtocol(self, addr):
        logger.debug("Building mythtv protocol.")
        p = self.protocol(self)
        return p
    
    def connected(self, endpoint):
        self.endpoint = endpoint
        self.mythclass.connected(self.deviceid, endpoint)

    def clientConnectionLost(self, connector, reason):
#        self.mythclass.disconnected()
        print 'Lost connection.  Reason:', reason
        ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

    def clientConnectionFailed(self, connector, reason):
        print 'Connection failed. Reason:', reason
        ReconnectingClientFactory.clientConnectionFailed(self, connector,
                                                         reason)

    def disconnected(self, reconnect):
#        self.mythclass.disconnected(reconnect)
        pass

    def dataReceived(self, msgID, status, data):
        """
        The protocol class this function when data is received from
        a mythtv front end.
        
        Simply pass this to mythclass for processing/logic.  MythTV class
        needs the deviceid and the data.
        """
        logger.debug("factory::dataReceived: %s", data)
        self.mythclass.dataReceived(msgID, self.deviceid, status, data)
