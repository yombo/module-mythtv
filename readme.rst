MythTV Interface
================

This code has been written in semi-pseudo code.  It hasn't been completed or tested. It was
written as a framework for someone to complete development.

Interface to MythTV frontends. Stop, play, pause, etc.

Installation
============

Simply mark this module as being used by the gateway, and the gateway will
download this module and install this module automatically.

Requirements
============

Working mythtv frontend.

License
=======

See LICENSE file for full details.
